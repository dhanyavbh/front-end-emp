export class Employee {
  id: number;
  firstName: string;
  lastName: string;
  department:string;
  dob:string;
  gender:String
}
