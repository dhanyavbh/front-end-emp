import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private router: Router) { }
  title = 'Angular 6 + Spring Boot 2 + Spring Data JPA + MySQL + CRUD Tutorial';
  gotoList() {
    console.log("inside");
    this.router.navigate(['/employees']);
  }
  gotoAdd(){
    this.router.navigate(['/add']);
  }
}
